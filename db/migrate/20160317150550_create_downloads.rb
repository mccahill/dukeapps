class CreateDownloads < ActiveRecord::Migration
  def change
    create_table :downloads do |t|
      t.string :netid
      t.string :package
      t.string :secretKey

      t.timestamps
    end
  end
end
