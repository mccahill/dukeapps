class AddSha1hashToPhoneuploads < ActiveRecord::Migration
  def change
    add_column :phoneuploads, :sha1hash, :string
  end
end
