class CreatePayloads < ActiveRecord::Migration
  def change
    create_table :payloads do |t|
      t.string :name
      t.string :plistTemplate
      t.string :ipaLocation

      t.timestamps
    end
  end
end
