class RemoveDescriptionFromPhoneuploads < ActiveRecord::Migration
  def up
    remove_column :phoneuploads, :description
  end

  def down
    add_column :phoneuploads, :description, :text
  end
end
