class RemoveNameFromPhoneuploads < ActiveRecord::Migration
  def up
    remove_column :phoneuploads, :name
  end

  def down
    add_column :phoneuploads, :name, :string
  end
end
