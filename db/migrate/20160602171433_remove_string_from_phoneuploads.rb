class RemoveStringFromPhoneuploads < ActiveRecord::Migration
  def up
    remove_column :phoneuploads, :string
  end

  def down
    add_column :phoneuploads, :string, :string
  end
end
