class CreatePhoneuploads < ActiveRecord::Migration
  def change
    create_table :phoneuploads do |t|
      t.string :name
      t.text :description
      t.string :uploadfile
      t.string :string

      t.timestamps
    end
  end
end
