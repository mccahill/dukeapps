class CreateAdmins < ActiveRecord::Migration
  def change
    create_table :admins do |t|
      t.string :netid
      t.string :displayName
      t.string :duid

      t.timestamps
    end
  end
end
