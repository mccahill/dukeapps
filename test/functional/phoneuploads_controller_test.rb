require 'test_helper'

class PhoneuploadsControllerTest < ActionController::TestCase
  setup do
    @phoneupload = phoneuploads(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:phoneuploads)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create phoneupload" do
    assert_difference('Phoneupload.count') do
      post :create, phoneupload: { sha1hash: @phoneupload.sha1hash, uploadfile: @phoneupload.uploadfile }
    end

    assert_redirected_to phoneupload_path(assigns(:phoneupload))
  end

  test "should show phoneupload" do
    get :show, id: @phoneupload
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @phoneupload
    assert_response :success
  end

  test "should update phoneupload" do
    put :update, id: @phoneupload, phoneupload: { description: @phoneupload.description, name: @phoneupload.name, string: @phoneupload.string, uploadfile: @phoneupload.uploadfile }
    assert_redirected_to phoneupload_path(assigns(:phoneupload))
  end

  test "should destroy phoneupload" do
    assert_difference('Phoneupload.count', -1) do
      delete :destroy, id: @phoneupload
    end

    assert_redirected_to phoneuploads_path
  end
end
