DukeApp::Application.routes.draw do
    

  resources :phoneuploads do as_routes end
  
  # the app_install heirarchy is not shib protected, so let's do our REST thing here
  post 'app_install/phoneupload' => 'phoneuploads#create'

  resources :payloads do as_routes end
  resources :downloads do as_routes end
  resources :sessions do as_routes end
  resources :users do as_routes end
  resources :admins do as_routes end
    
  get 'app_install/install_parkduke'
  get 'app_install/small_image_parkduke'
  get 'app_install/large_image_parkduke'
  get 'app_install/ipa_parkduke'

  get 'app_install/install_wearduke'
  get 'app_install/small_image_wearduke'
  get 'app_install/large_image_wearduke'
  get 'app_install/ipa_wearduke'

  get 'app_install/install_reengage'
  get 'app_install/small_image_reengage'
  get 'app_install/large_image_reengage'
  get 'app_install/ipa_reengage'

  get 'app_install/install_wearduketest'
  get 'app_install/install_wearduketest'
  get 'app_install/small_image_wearduketest'
  get 'app_install/large_image_wearduketest'
  get 'app_install/ipa_wearduketest'

  get 'app_install/install_s2k'
  get 'app_install/small_image_s2k'  
  get 'app_install/large_image_s2k'
  get 'app_install/ipa_s2k'

  get 'app_install/install_dukedoor'
  get 'app_install/small_image_dukedoor'
  get 'app_install/large_image_dukedoor'
  get 'app_install/ipa_dukedoor'
  
    
  # send a plist to the user
  get 'install_plist' => 'home#install_plist'
  get 'install_ipa' => 'home#install_ipa'
  get 'install' => 'home#install_ipa'      
      
  get "administration/index"
  
  get "home/index"
  match 'home/logout'=>'home#logout'
  get 'home/log_the_parkduke_app_install'
  get 'home/log_the_wearduke_app_install'
  get 'home/log_the_reengage_app_install'
  get 'home/log_the_wearduketest_app_install'
  get 'home/log_the_s2k_app_install'
  get 'home/log_the_dukedoor_app_install'
  
  # convenient way to get a dump of the session and environment
  get 'debug' => 'home#debug'

  root :to => 'home#index'


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
