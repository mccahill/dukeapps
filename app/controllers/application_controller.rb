class ApplicationController < ActionController::Base
  protect_from_forgery
  
  layout "base"
  
  before_filter :authorize

  private
  def authorize
    
    if ENV['RAILS_ENV'] == 'development'
      # duid = '0198623'
      @duid = '0435319'
      @netid = 'mccahill'
      @displayName = 'Mark McCahill'
      @affiliation = 'staff'
    else
      @duid = request.env['unique-id']
      @netid = request.env['eppn'].split("@").first
      @displayName = request.env['displayName']
      @affiliation = request.env['affiliation'].split("@").first
    end
    
    session[:parkduke_people] = ((@netid == "mccahill") || (@netid == "hwt2"))
    
    # should be using isMemberOf here for the group assertion
    #   urn:mace:duke.edu:groups:policies:research_services:wearduke_participants
    groups = (request.env['ismemberof'] || '').split(';')
    session[:wearduke_people]  = (( groups.include? 'urn:mace:duke.edu:groups:policies:research_services:wearduke_participants' ) || (@netid == "hwt2") || (@netid == "sme39") || (@netid == "bjy12") || (@netid == "rjs25")  || (@netid == "sbhaga"))
    
    # session[:wearduke_people] = ((@netid == "mccahill") || (@netid == "hwt2") || (@netid == "sme39") || (@netid == "ck129") || (@netid == "bjy12") || (@netid == "rjs25")  || (@netid == "sbhaga"))
    
    # enable Duke ReEngage a.k.a. SymMon for now
    session[:reengage_people]  = ( groups.include? 'urn:mace:duke.edu:groups:group-manager:roles:duke-reengage-app-test' )

    session[:wearduketest_people] = ((@netid == "mccahill") || (@netid == "hwt2") || (@netid == "pal28") || (@netid == "ck129") || (@netid == "emk52") || (@netid == "jimwood") || (@netid == "jmh147") || (@netid == "sme39") || (@netid == "saw38") || (@netid == "bjy12") || (@netid == "rjs25")  || (@netid == "sbhaga"))
    
    session[:s2k_people] = ((@netid == "mccahill") || (@netid == "hwt2") || (@netid == "sme39")) 
    
    @this_user = User.find_or_create_by_duid(duid: @duid)
    @this_user.netid = @netid
    @this_user.displayName = @displayName
    @this_user.save
    
    session[:user_id] = @duid        
    if Admin.find_by_netid( @netid )
      session[:is_admin] = true
    else
      session[:is_admin] = false
    end
  end    
	def onlyAdmin
    unless( session[:is_admin])
			flash[:error] = "You do not have access to that function"
      logger.warn "User #{session[:user_id]} tried to access an Admin-Only Controller: #{params[:controller]}"
      redirect_to "/"
    end
	end
  
end
