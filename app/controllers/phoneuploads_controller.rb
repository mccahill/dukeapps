class PhoneuploadsController < ApplicationController
  
  ##############
  # generally we assume that apps will be programatically posting files here
  # instead of users visiting the web pages, so this is mostly a REST-style interface
  # You can validate that this works via a CURL command something like this:
  #
  #      curl -X POST \
  #        -H "Content-Type: multipart/form-data" \
  #        -H "Accept: application/json"
  #        --form "sha1hash=put-the-hash-for-the-file-here" \
  #        --form "uploadfile=@filname.zip" \
  #        http://localhost:3000/phoneuploads
  #
  # the json returned will look like this:
  #
  #     [{"status":"OK"},{"created_at":"2016-06-05T21:38:41Z"}]
  #
  #  or like this in the event of a failure:
  #
  #     [{"status":"ERROR"},
  #          {"ERROR":["SHA1 hash 36454383da993f976f5c22e6d73856f46c2ec044 sent with file does 
  #                     not match hash calculated on file received at server:
  #                     36454383da993f976f5c22e6d73856f46c2ec045"]
  #           }]
  #
  # this consistency checking on the SHA-1 hash of the file happens before it is saved
  # on the server, so you cannot upload without a valid hash, and we have some assurance
  # that the file arrived without damage.
  #############
  
  
  # GET /phoneuploads/new
  # GET /phoneuploads/new.json
  def new
    @phoneupload = Phoneupload.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @phoneupload }
    end
  end


  # POST /phoneuploads
  # POST /phoneuploads.json
  def create
    @phoneupload = Phoneupload.new({:sha1hash => params[:sha1hash], :uploadfile => params[:uploadfile]})

    respond_to do |format|
      if @phoneupload.save
        Session.create(:action => 'phoneupload-OK', :netid => '', :notes => "#{@phoneupload.sha1hash} - #{@phoneupload.uploadfile}")
        format.html { redirect_to @phoneupload, notice: 'Phoneupload was successfully created.' }
        format.json { render json: [{:status => 'OK'}, :created_at => @phoneupload.created_at], status: :created }
#        format.json { render json: @phoneupload, status: :created, location: @phoneupload }
      else
        Session.create(:action => 'phoneupload-ERROR', :netid => '', :notes => "#{@phoneupload.errors.first} - #{@phoneupload.uploadfile}")
        format.html { render action: "new" }
        format.json { render json: [{:status => 'ERROR'}, @phoneupload.errors], status: :unprocessable_entity }      
#        format.json { render json: @phoneupload.errors, status: :unprocessable_entity }
      end
    end
  end
  
  #############################################################
  # need to fix this to limit the vewing of previously uploaded stuff to admins only
  # but in the interests of getting the iOS developer working doing their
  # uploads we will not bother right now
  #
  # this should be limit access to everything except uploads to the app admins
  # so - everything below this line is app admin only
  #############################################################
  
  #before_filter :onlyAdmin  
  layout 'admin_base'
  
  active_scaffold :"phoneupload" do |conf|
    conf.actions = [:list, :search, :show, :delete]
    active_scaffold_config.search.live = true   #submit search terms as we type for more feedback
    conf.list.sorting = { :id => :desc}
    
    conf.columns = [ :id, :sha1hash, :uploadfile, :updated_at, :created_at ] 
    conf.columns[:sha1hash].label = "SHA-1 hash"      
    conf.columns[:id].label = "id"      
    conf.columns[:uploadfile].label = "file"      
  end

	# Override authorize in application_controller.rb
  def authorize
    case request.format
    when Mime::JSON
      return true
    else
      super
    end
  end


end
