class PayloadsController < ApplicationController
  layout 'admin_base'
  before_filter :onlyAdmin
  
  active_scaffold :"payload" do |conf|
    conf.actions = [:list, :search, :show]
    active_scaffold_config.search.live = true   #submit search terms as we type for more feedback
    conf.list.sorting = { :id => :asc}
    
    conf.columns = [ :id, :ipaLocation, :plistTemplate, :updated_at, :created_at ]  
    conf.columns[:name].label = "app name"      
    conf.columns[:ipaLocation].label = "package"      
    conf.columns[:plistTemplate].label = "plist template"      
    conf.columns[:id].label = "id"                                 
  end
end
