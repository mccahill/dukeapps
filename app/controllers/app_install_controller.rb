class AppInstallController < ApplicationController
    
  # return the manifest (a.k.a the plist) for the ParkDuke iOS app
  def install_parkduke
    file = File.join(Rails.root, 'app', 'iosfiles', 'parkduke', 'manifest.plist')
    @manifest = File.read(file)
    @use_json = 'This is an iOS app - to install this app, use Sarai on your iOS device to visit this site.'
    respond_to do |format|
      format.html { render :layout => false, :json => @manifest }
      format.json { render :layout => false, :json => @manifest }
    end 
  end

  # return the manifest (a.k.a the plist) for the WearDuke iOS app
  def install_wearduke
    file = File.join(Rails.root, 'app', 'iosfiles', 'wearduke', 'manifest.plist')
    @manifest = File.read(file)
    @use_json = 'This is an iOS app - to install this app, use Sarai on your iOS device to visit this site.'
    respond_to do |format|
      format.html { render :layout => false, :json => @manifest }
      format.json { render :layout => false, :json => @manifest }
    end 
  end

  # return the manifest (a.k.a the plist) for the Duke ReEngage iOS app
  def install_reengage
    file = File.join(Rails.root, 'app', 'iosfiles', 'reengage', 'manifest.plist')
    @manifest = File.read(file)
    @use_json = 'This is an iOS app - to install this app, use Sarai on your iOS device to visit this site.'
    respond_to do |format|
      format.html { render :layout => false, :json => @manifest }
      format.json { render :layout => false, :json => @manifest }
    end 
  end

  # return the manifest (a.k.a the plist) for the WearDukeTest iOS app
  def install_wearduketest
    file = File.join(Rails.root, 'app', 'iosfiles', 'wearduketest', 'manifest.plist')
    @manifest = File.read(file)
    @use_json = 'This is an iOS app - to install this app, use Sarai on your iOS device to visit this site.'
    respond_to do |format|
      format.html { render :layout => false, :json => @manifest }
      format.json { render :layout => false, :json => @manifest }
    end 
  end

  # return the manifest (a.k.a the plist) for the s2k iOS app
  def install_s2k
    file = File.join(Rails.root, 'app', 'iosfiles', 's2k', 'manifest.plist')
    @manifest = File.read(file)
    @use_json = 'This is an iOS app - to install this app, use Sarai on your iOS device to visit this site.'
    respond_to do |format|
      format.html { render :layout => false, :json => @manifest }
      format.json { render :layout => false, :json => @manifest }
    end 
  end

  # return the manifest (a.k.a the plist) for the DukeDoor iOS app
  def install_dukedoor
    file = File.join(Rails.root, 'app', 'iosfiles', 'dukedoor', 'manifest.plist')
    @manifest = File.read(file)
    @use_json = 'This is an iOS app - to install this app, use Sarai on your iOS device to visit this site.'
    respond_to do |format|
      format.html { render :layout => false, :json => @manifest }
      format.json { render :layout => false, :json => @manifest }
    end 
  end
  
  # the small iOS app icon for the ParkDuke iOS app
  def small_image_parkduke
    stream_a_file('parkduke','icon_small_image_parkduke.png')
  end

  # the small iOS app icon for the WearDuke iOS app
  def small_image_wearduke
    stream_a_file('wearduke','icon_small_image_wearduke.png')
  end

  # the small iOS app icon for the Duke ReEnagage iOS app
  def small_image_reengage
    stream_a_file('reengage','icon_small_image_reengage.png')
  end

  # the small iOS app icon for the WearDukeTest iOS app
  def small_image_wearduketest
    stream_a_file('wearduketest','icon_small_image_wearduketest.png')
  end

  # the small iOS app icon for the s2k iOS app
  def small_image_s2k
    stream_a_file('s2k','icon_small_image_s2k.png')
  end

  # the small iOS app icon for the DukeDoor iOS app
  def small_image_dukedoor
    stream_a_file('dukedoor','icon_small_image_dukedoor.png')
  end

  # the large iOS app icon for the ParkDuke iOS app
  def large_image_parkduke
    stream_a_file('parkduke','icon_large_image_parkduke.png')
  end
    
  # the large iOS app icon for the WearDuke iOS app
  def large_image_wearduke
    stream_a_file('wearduke','icon_large_image_wearduke.png')
  end
    
  # the large iOS app icon for the Duke ReEngage iOS app
  def large_image_reengage
    stream_a_file('reengage','icon_large_image_reengage.png')
  end
    
  # the large iOS app icon for the WearDukeTest iOS app
  def large_image_wearduketest
    stream_a_file('wearduketest','icon_large_image_wearduketest.png')
  end
    
  # the large iOS app icon for the s2k iOS app
  def large_image_s2k
    stream_a_file('s2k','icon_large_image_s2k.png')
  end
    
  # the large iOS app icon for the DukeDoor iOS app
  def large_image_dukedoor
    stream_a_file('dukedoor','icon_large_image_dukedoor.png')
  end
    
  # the  iOS .ipa file for the ParkDuke iOS app
  def ipa_parkduke
    stream_a_file('parkduke','parkduke.ipa')
  end
  
  # the  iOS .ipa file for the WearDuke iOS app
  def ipa_wearduke
    stream_a_file('wearduke','wearduke.ipa')
  end
  
  # the  iOS .ipa file for the Duke ReEngage iOS app
  def ipa_reengage
    stream_a_file('reengage','reengage.ipa')
  end
  
  # the  iOS .ipa file for the WearDukeTest iOS app
  def ipa_wearduketest
    stream_a_file('wearduketest','wearduketest.ipa')
  end
  
  # the  iOS .ipa file for the s2k iOS app
  def ipa_s2k
    stream_a_file('s2k','s2k.ipa')
  end
  
  # the  iOS .ipa file for the DukeDoor iOS app
  def ipa_dukedoor
    stream_a_file('dukedoor','dukedoor.ipa')
  end
  
  def stream_a_file( dir_name, f_name )
    a_file = File.join(Rails.root, 'app', 'iosfiles', dir_name, f_name )
    # stream a file to the client with default type of application/octet-stream
    file_contents = File.read(a_file)
    send_data file_contents, :filename => f_name  
  end
 

  private  
  
  # Override authorize in application_controller.rb
  def authorize
  end
  
  
end
