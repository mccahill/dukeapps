class DownloadsController < ApplicationController
  layout 'admin_base'
  before_filter :onlyAdmin
  
  active_scaffold :"download" do |conf|
    conf.actions = [:list, :search, :show]
    active_scaffold_config.search.live = true   #submit search terms as we type for more feedback
    conf.list.sorting = { :id => :desc}
    
    conf.columns = [ :id, :netid, :package, :secretKey, :updated_at, :created_at ]  
    conf.columns[:secretKey].label = "download key"      
    conf.columns[:netid].label = "netid"      
    conf.columns[:id].label = "id"      
    conf.columns[:package].label = "package"            
  end
end
