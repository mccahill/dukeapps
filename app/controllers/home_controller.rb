class HomeController < ApplicationController

  layout "home"
  
  def index
  end
  
  def logout
     reset_session
     return_to_me = "?returnto=" + url_for(:controller=>'home')
     redirect_to "/Shibboleth.sso/Logout?return=https://shib.oit.duke.edu/cgi-bin/logout.pl" +
       CGI.escape(return_to_me)
   end

   def log_the_parkduke_app_install
     # write an entry to the sessions table
     Session.create(:action => 'Install-ParkDuke', 
                    :netid => "#{@netid}", 
                    :notes => "#{request.env['REMOTE_ADDR']} - #{request.env['HTTP_USER_AGENT']} ")
    
     @http_server_type = 'https'
     if ENV['RAILS_ENV'] == 'development' # during dev we are not on an https server...
       @http_server_type = 'http' 
     end
    
     # now send them off to get the manifest
     redirect_to "itms-services://?action=download-manifest&amp;url=#{@http_server_type}://#{request.env['HTTP_HOST']}/app_install/install_parkduke"         
   end

   def log_the_wearduke_app_install
     # write an entry to the sessions table
     Session.create(:action => 'Install-WearDuke', 
                    :netid => "#{@netid}", 
                    :notes => "#{request.env['REMOTE_ADDR']} - #{request.env['HTTP_USER_AGENT']} ")
    
     @http_server_type = 'https'
     if ENV['RAILS_ENV'] == 'development' # during dev we are not on an https server...
       @http_server_type = 'http' 
     end
    
     # now send them off to get the manifest
     redirect_to "itms-services://?action=download-manifest&amp;url=#{@http_server_type}://#{request.env['HTTP_HOST']}/app_install/install_wearduke"         
   end

   def log_the_wearduketest_app_install
     # write an entry to the sessions table
     Session.create(:action => 'Install-WearDukeTest', 
                    :netid => "#{@netid}", 
                    :notes => "#{request.env['REMOTE_ADDR']} - #{request.env['HTTP_USER_AGENT']} ")
    
     @http_server_type = 'https'
     if ENV['RAILS_ENV'] == 'development' # during dev we are not on an https server...
       @http_server_type = 'http' 
     end
    
     # now send them off to get the manifest
     redirect_to "itms-services://?action=download-manifest&amp;url=#{@http_server_type}://#{request.env['HTTP_HOST']}/app_install/install_wearduketest"         
   end

   def log_the_reengage_app_install
     # write an entry to the sessions table
     Session.create(:action => 'Install-ReEnage', 
                    :netid => "#{@netid}", 
                    :notes => "#{request.env['REMOTE_ADDR']} - #{request.env['HTTP_USER_AGENT']} ")
    
     @http_server_type = 'https'
     if ENV['RAILS_ENV'] == 'development' # during dev we are not on an https server...
       @http_server_type = 'http' 
     end
    
     # now send them off to get the manifest
     redirect_to "itms-services://?action=download-manifest&amp;url=#{@http_server_type}://#{request.env['HTTP_HOST']}/app_install/install_reengage"         
   end

   def log_the_s2k_app_install
     # write an entry to the sessions table
     Session.create(:action => 'Install-s2k', 
                    :netid => "#{@netid}", 
                    :notes => "#{request.env['REMOTE_ADDR']} - #{request.env['HTTP_USER_AGENT']} ")
    
     @http_server_type = 'https'
     if ENV['RAILS_ENV'] == 'development' # during dev we are not on an https server...
       @http_server_type = 'http' 
     end
    
     # now send them off to get the manifest
     redirect_to "itms-services://?action=download-manifest&amp;url=#{@http_server_type}://#{request.env['HTTP_HOST']}/app_install/install_s2k"         
   end

   def log_the_dukedoor_app_install
     # write an entry to the sessions table
     Session.create(:action => 'Install-DukeDoor', 
                    :netid => "#{@netid}", 
                    :notes => "#{request.env['REMOTE_ADDR']} - #{request.env['HTTP_USER_AGENT']} ")
    
     @http_server_type = 'https'
     if ENV['RAILS_ENV'] == 'development' # during dev we are not on an https server...
       @http_server_type = 'http' 
     end
    
     # now send them off to get the manifest
     redirect_to "itms-services://?action=download-manifest&amp;url=#{@http_server_type}://#{request.env['HTTP_HOST']}/app_install/install_dukedoor"         
   end

   
   def install_plist
     file_path = "#{Rails.root}/public/testing/Example.plist"
     send_file file_path, :filename => params[:filename], :disposition => 'attachment'     
   end

   def install_ipa
     file_path = "#{Rails.root}/public/testing/Example.ipa"
     send_file file_path, :filename => params[:filename], :disposition => 'attachment'     
   end

end
