class SessionsController < ApplicationController
  layout 'admin_base'
  before_filter :onlyAdmin
  
  active_scaffold :"session" do |conf|
    conf.actions = [:list, :search, :show]
    active_scaffold_config.search.live = true   #submit search terms as we type for more feedback
    conf.list.sorting = { :id => :desc}
    
    conf.columns = [ :id, :netid, :action, :notes, :updated_at, :created_at ] 
    conf.columns[:netid].label = "netid"      
    conf.columns[:id].label = "id"      
  end
end
