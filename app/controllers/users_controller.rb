class UsersController < ApplicationController
  layout 'admin_base'
  before_filter :onlyAdmin
  
  active_scaffold :"user" do |conf|
    conf.actions = [:list, :search, :show]
    active_scaffold_config.search.live = true   #submit search terms as we type for more feedback
    conf.list.sorting = { :id => :desc}
    
    conf.columns = [ :id, :netid, :displayName, :duid, :updated_at, :created_at ]  
    conf.columns[:displayName].label = "name"   
    conf.columns[:netid].label = "netid"      
    conf.columns[:id].label = "id"      
    conf.columns[:duid].label = "Duke unique id"   
  end
end
