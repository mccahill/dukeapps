class Phoneupload < ActiveRecord::Base
  attr_accessible :uploadfile, :sha1hash
  mount_uploader :uploadfile, UploadfileUploader
  
  before_validation :check_sha1_hash , :on => :create

  require 'digest/sha1'
  
  def compute_sha1_hash
      hash = Digest::SHA1.new
      open("#{self.uploadfile}", 'r') do |io|
        until io.eof?
          buffer = io.read(1024)
          hash.update(buffer)
        end
      end
      # return hash.base64digest
      return hash.hexdigest   
  end

  def check_sha1_hash
    @the_hash = compute_sha1_hash
    if @the_hash == self.sha1hash
      return true
    else
      errors.add(:ERROR, "SHA1 hash #{self.sha1hash} sent with file does not match hash #{@the_hash} calculated on file received at server: #{@the_hash}")
      return false
    end
  end
           
end
